#include <ros/ros.h>
#include <iostream>
#include <string>
#include <image_transport/image_transport.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include <alproxies/alvideodeviceproxy.h>
#include <alvision/alimage.h>
#include <alvision/alvisiondefinitions.h>
#include <alerror/alerror.h>


using namespace cv;

void grabImages(const std::string& robotIp, ros::Publisher& pub);

int main(int argc, char** argv)
{
    ros::init(argc, argv, "testTheStrange");
    
    ros::NodeHandle nh;
    ros::Publisher pub = nh.advertise<sensor_msgs::Image>("image_node/nao_pictures", 1);
//    image_transport::ImageTransport imageTr(nh);

//    image_transport::Publisher pub = imageTr.advertise("image_node/nao_pictures", 1);

    if (argc < 2)
    {
        std::cerr << "Usage 'getimages robotIp'" << std::endl;
        return 1;
    }
    const std::string robotIp(argv[1]);
std::cout << robotIp << std::endl;

    try{
//        std::cout << "do nothing" << std::endl;
        grabImages(robotIp, pub);
    }
    catch (const AL::ALError& e){
        std::cerr << "Caught exception: " << e.what() << std::endl;
    }
    
    
    std::cout << "Strange things are going to happen" << std::endl;
    return 0;
}

void grabImages(const std::string& robotIp, ros::Publisher& pub){
    sensor_msgs::Image sensorImg;
    AL::ALVideoDeviceProxy camProxy(robotIp, 9559);
    //    camProxy::setResolution(robotIp, AL::kVGA);
    
    /** Subscribe a client image requiring 320*240 and BGR colorspace.*/
    const std::string clientName = camProxy.subscribe("test", AL::kVGA, AL::kBGRColorSpace, 30);

    /** Create an cv::Mat header to wrap into an opencv image.*/
    cv::Mat imgHeader = cv::Mat(cv::Size(640, 480), CV_8UC3);

    /** Main loop. Exit when pressing ESC.*/
    while ((char) cv::waitKey(30) != 27)
    {
        /** Retrieve an image from the camera.
        * The image is returned in the form of a container object, with the
        * following fields:
        * 0 = width
        * 1 = height
        * 2 = number of layers
        * 3 = colors space index (see alvisiondefinitions.h)
        * 4 = time stamp (seconds)
        * 5 = time stamp (micro seconds)
        * 6 = image buffer (size of width * height * number of layers)
        */
        AL::ALValue img = camProxy.getImageRemote(clientName);

        /** Access the image buffer (6th field) and assign it to the opencv image
        * container. */
        imgHeader.data = (uchar*) img[6].GetBinary();

        /** Tells to ALVideoDevice that it can give back the image buffer to the
        * driver. Optional after a getImageRemote but MANDATORY after a getImageLocal.*/
        camProxy.releaseImage(clientName);

        cv_bridge::CvImage cv_image;
        cv_image.encoding = sensor_msgs::image_encodings::TYPE_8UC3;
        cv_image.image = imgHeader;
        pub.publish(cv_image.toImageMsg());
    }
    /** Cleanup.*/
    camProxy.unsubscribe(clientName);
}
