# README #
# 
This is the **ECP object detection module**. 

For starters I added the boost library directly into the project as I had version issues with my preinstalled boost (need at least 1.53.0 for boost.Atomic). If you can use your system-boost, just change the line in  src/ffld/CMakeLists.txt in the include_directories to include the path to the boost library and remove the boost from the local source folder.

Other dependencies are: (please tell me (stefan@kinauer.de) if I missed one (besides something like a c++ compiler, ros or catkin))

* Eigen (included in the source folder)
* FFTW (maybe to be installed with ./configure --enable-shared)
* JPEG 
* LibXml2 
* OpenMP

To be completed (cleaning up conflicts with the NAO SDK)...

The project depends upon reconfig_common_msgs package in aalto repository 

* *$ cd ../catkin_workspace/src (any where under this source folder)* 
* *$ git clone https://<YOURUSERNAME>@bitbucket.org/eu-reconfig/aalto.git*
* *$ git clone https://<YOURUSERNAME>@bitbucket.org/eu-reconfig/ecp.git*
* *$ cd ../catkin_workspace/*
* *$ catkin_make*

**How does the interface look like, how do I run it?**

This module's purpose is to detect objects specified by a model which was trained offline before. So far the module contains two models (for the toy car and the ball), but could be extended to further objects.
As input the algorithm expects an rgb-image and the object class to be detected and outputs a vector containing all detected objects described by a detection score (a confidence, not a real probability), it's position and size in pixel coordinates of the input image. The object class is given by a string in the field *ObjClass* of the input structure (defined in *AALTO/reconfig_common_msgs/srv/ObjectDetectPx.srv*). The output structure is defined in the aalto/reconfig_common_msgs node. An example stub (service_stub) has been added to the repository, showing the result of a request.
The following commands have to be issued to make the detection work. Always source the devel/setup.bash file. Then startup the *roscore*. Next you can start the following nodes (order not necessary) for the standalone test:

* *rosrun ffld dpm_detector_srv*
* *rosrun image_node image_node_srv*
* *rosrun image_node image_node_client [path to a input picture]*

The image_node_client posts repeatedly the given file to a topic which is read by the image_node_srv. This node publishes the actual image and is received by the dpm_detector_srv. This node does nothing with these images for now (except for outputting text on the console ;) until it receives the command to detect an object. To issue this command, you can run: *rosrun ffld service_stub*. See in *ffld/src/service_stub.cpp* for example code.
NOTE:  [path to a input picture] is complete path to an image. Its not image name in the current directory

The code can also be triggered using aalto package nao_compute_3d by executing 
$ rosrun nao_compute_3d testdmpserver_node 

To run image_node_nao_getimages set bash environment variable NAOqi_DIR to root of NAO SDK 

* *$ export NAOqi_DIR=<Path to NAO_SDK>*

The config file (*ffld/data/test.cfg*) contains the available models with their string-key needed for the service structure in the first column.