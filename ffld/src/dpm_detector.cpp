#include <algorithm>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <vector>
#include <time.h>

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/contrib/contrib.hpp"

#include <ros/ros.h>
#include <ros/package.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "CImageConventor.h"

#include "reconfig_common_msgs/ObjectDetectPx.h"                // srv
#include "reconfig_common_msgs/DetectResult2dPx.h"              // msg
#include "reconfig_common_msgs/ObjClass.h"                      // msg
#include "reconfig_common_msgs/Point2d.h"                       // msg
#include "reconfig_common_msgs/Rectangle2d.h"                   // msg

// these are from the old format of detection
#include "reconfig_common_msgs/detect_res.h"
#include "reconfig_common_msgs/det_list.h"
#include "reconfig_common_msgs/point.h"
#include "reconfig_common_msgs/rect.h"


// marker1 ffld usage
#include <SimpleOpt.h>
#include <Intersector.h>
#include <Mixture.h>
#include <Scene.h>
#include <HOGPyramid.h>
#include <Patchwork.h>
#include <sys/time.h>

#define BILLION  1E9

using namespace std;
using namespace cv;
using namespace ros;

typedef struct DetectionResult
{
	Rect rtRoot;
	std::vector<Rect> vParts;
}_detectionResult_;

struct Detection : public FFLD::Rectangle
{
	FFLD::HOGPyramid::Scalar score;
	int l;
	int x;
	int y;

	Detection() : score(0), l(0), x(0), y(0)
	{
	}

	Detection(FFLD::HOGPyramid::Scalar score, int l, int x, int y, FFLD::Rectangle bndbox) :
		FFLD::Rectangle(bndbox), score(score), l(l), x(x), y(y)
	{
	}

	bool operator<(const Detection & detection) const
	{
		return score > detection.score;
	}
};

// Global variable
cv::Mat 		g_matImg;
vector<string>  g_vXMLFiles;
int 			g_nThreads;
float			g_fThreshold;
string strPackagePath;

void imageCvtCallback( const sensor_msgs::ImageConstPtr& msg );
void readConfigFile( string strFilename );
static void detectAndDrawObjects( Mat& image, LatentSvmDetector& detector, const vector<Scalar>& colors, float overlapThreshold, int numThreads );
void detect(const FFLD::Mixture & mixture, int width, int height, const FFLD::HOGPyramid & pyramid,  double threshold, double overlap, const string image, ostream & out, const string & images, vector<Detection> & detections);
int stop();

int main(int argc, char** argv)
{
	ros::init(argc, argv, "dpm_detector");
	strPackagePath = ros::package::getPath("ffld");
	string strCfgFilename = strPackagePath + "/data/test.cfg";
	readConfigFile( strCfgFilename );

	g_nThreads = 12;
	g_fThreshold = 0.5f;

	ros::NodeHandle rosHandle;
	image_transport::ImageTransport 	rosImageTrans( rosHandle );
	image_transport::Subscriber		rosImageSub;

	rosImageSub = rosImageTrans.subscribe( "/image_node/output_image", 1, imageCvtCallback );
	std::cout << "dpm detector is running" << std::endl;
	ros::spin();

	return 0;
}

void readConfigFile( string strFilename )
{
	string line;
	ifstream file( strFilename.c_str() );

	if( file.is_open() )
	{
		int cnt = 0;
		while( getline( file, line ) )
		{
			g_vXMLFiles.push_back( strPackagePath + line );
			std::cout << "name of xmlFile: " << strPackagePath << line << std::endl;
		}
		std::cout << "config file read" << std::endl;
	}
	else
	{
		ROS_ERROR("Unable to open config file.");
	}
}

void imageCvtCallback( const sensor_msgs::ImageConstPtr& msg )
{
	cv_bridge::CvImagePtr cv_ptr;

	ros::NodeHandle rosHandle;
	ros::Publisher det_pub = rosHandle.advertise<reconfig_common_msgs::det_list>( "detected_list", 1000 );

	try
	{
		cv_ptr = cv_bridge::toCvCopy( msg, sensor_msgs::image_encodings::TYPE_8UC3 );
	}
	catch (cv_bridge::Exception& e)
	{
		ROS_ERROR("cv_bridge exception: %s", e.what());
		return;
	}

	// let the fun begin
	using namespace FFLD;
	using namespace std;

	double threshold = 0.5;
	int padding = 12;
	double overlap = 0.3;
	int interval = 10;
	const string file(strPackagePath + "/data/temp/input.jpg");
	const string images(strPackagePath + "/data/output");
	const string resultsFile(strPackagePath + "/data/output/results.txt");

	// for time measurements
	struct timespec start_t,finish_t;
	double time_taken, total_time = 0.0;
	clock_gettime(CLOCK_REALTIME,&start_t);

	g_matImg = cv_ptr->image.clone();
	bool success = imwrite(file, g_matImg);
	if (success){
		std::cout << "image written to temp" << std::endl;
	}
	else{
		std::cout << "check if folder " << strPackagePath << "/data/temp exists!" << std::endl;
		return;
	}

	// read image file into the right format
	JPEGImage image(file);
	if (image.empty()) {
		std::cout << "Temp file not found or invalid" << std::endl;
		return;
	}

	// Try to open the model
	ifstream in(g_vXMLFiles.at(0).c_str(), ios::binary);

	if (!in.is_open()) {
		std::cout << "Invalid model file " << g_vXMLFiles.at(0) << std::endl;
		return;
	}

	Mixture mixture;
	in >> mixture;

	if (mixture.empty()) {
		std::cout << "Invalid model file " << g_vXMLFiles.at(0) << std::endl;
		return;
	}

	// Try to open the results
	ofstream out;
	out.open(resultsFile.c_str(), ios::binary);

	if (!out.is_open()) {
		std::cout << "Invalid results file " << resultsFile << std::endl;
		return;
	}

	// Compute the HOG features
	start();

	HOGPyramid pyramid(image, padding, padding, interval);

	if (pyramid.empty()) {
		std::cout << "Invalid image " << strPackagePath << "/data/temp/input.jpg" << std::endl;
		return;
	}

	std::cout << "Computed HOG features in " << stop() << " ms" << std::endl;

	// Initialize the Patchwork class
	start();

	if (!Patchwork::Init((pyramid.levels()[0].rows() - padding + 15) & ~15, (pyramid.levels()[0].cols() - padding + 15) & ~15)) {
		std::cout << "Could not initialize the Patchwork class" << std::endl;
		return;
	}

	std::cout << "Initialized FFTW in " << stop() << " ms" << std::endl;

	start();

	mixture.cacheFilters();

	std::cout << "Transformed the filters in " << stop() << " ms" << std::endl;

	// Compute the detections
	start();
	vector<Detection> detections;


	detect(mixture, image.width(), image.height(), pyramid, threshold, overlap, file, out, images, detections);

	clock_gettime(CLOCK_REALTIME,&finish_t);
	time_taken = (finish_t.tv_sec - start_t.tv_sec) + (finish_t.tv_nsec - start_t.tv_nsec)/BILLION;
	cout<<"Detection took "<<time_taken<<" seconds."<<endl;

	std::cout << "Computed the convolutions and distance transforms in " << stop() << " ms" << std::endl;

	/*
    vector<string> vModelFiles;
    for( int i = 0; i < g_vXMLFiles.size(); i ++ )
    {
 	   vModelFiles.push_back( g_vXMLFiles.at(i) );
    }
std::cout << vModelFiles.size() << " model file(s) loaded" << std::endl;
    vector<Scalar> vColors;
    generateColors( vColors, vModelFiles.size() );

	LatentSvmDetector detector( vModelFiles );
    vector<LatentSvmDetector::ObjectDetection> detections;
std::cout << "image size" << detectedImg.size() << std::endl;
    struct timespec start_t,finish_t;
    double time_taken, total_time = 0.0;
    clock_gettime(CLOCK_REALTIME,&start_t);

    detector.detect( detectedImg, detections, 0.5, 4 );

    clock_gettime(CLOCK_REALTIME,&finish_t);
    time_taken = (finish_t.tv_sec - start_t.tv_sec) + (finish_t.tv_nsec - start_t.tv_nsec)/BILLION;

    cout<<"Detection took "<<time_taken<<" seconds."<<endl;
std::cout << "detector found " << detections.size() << " results." << std::endl;
    for( size_t i = 0; i < detections.size(); i++ )
    {
        const LatentSvmDetector::ObjectDetection& od = detections[i];
        rectangle( detectedImg, od.rect, vColors.at( od.classID ), 2 );

     	reconfig_common_msgs::detect_res res;
     	res.class_id = od.classID;
     	res.score = od.score;
     	res.rt.x = od.rect.x;
     	res.rt.y = od.rect.y;
     	res.rt.width = od.rect.width;
     	res.rt.height = od.rect.height;
     	res.cen_pt.x = ( od.rect.x + od.rect.width ) / 2;
     	res.cen_pt.y = ( od.rect.y + od.rect.height ) / 2;

     	detected_pub.det_list.push_back( res );

     	ROS_INFO_STREAM( "ClassID:" << od.classID << " Score:"<< od.score << " Centre(x,y):"
     		<< ( od.rect.x + od.rect.width ) / 2 << ", " << ( od.rect.y + od.rect.height ) / 2);
    }

    imshow( "Result", detectedImg );
    waitKey(20);*/

	reconfig_common_msgs::ObjectDetectPx::Response response;
	for(size_t i = 0; i < detections.size(); i++ )
	{
		reconfig_common_msgs::DetectResult2dPx res;
		res.ObjClass = "objectclassstring";
		res.detScore = (float)detections.at(i).score;
		res.bBox.upperLeft.x = detections.at(i).x;
		res.bBox.upperLeft.y = detections.at(i).y;
		res.bBox.lowerRight.x = detections.at(i).right();
		res.bBox.lowerRight.y = detections.at(i).bottom();
		res.centroid.x = (res.bBox.upperLeft.x + res.bBox.lowerRight.x)/2;
		res.centroid.y = (res.bBox.upperLeft.y + res.bBox.lowerRight.y)/2;

		response.detected_objects.push_back( res );
	}
	//    det_pub.publish( detected_pub );
}



// marker1 everything past here
timeval Start, Stop;


inline void start()
{
	gettimeofday(&Start, 0);
}

int stop()
{
	gettimeofday(&Stop, 0);

	timeval duration;
	timersub(&Stop, &Start, &duration);

	return duration.tv_sec * 1000 + (duration.tv_usec + 500) / 1000;
}

using namespace FFLD;
using namespace std;


// SimpleOpt array of valid options
enum
{
	OPT_HELP, OPT_MODEL, OPT_NAME, OPT_RESULTS, OPT_IMAGES, OPT_NB_NEG, OPT_PADDING, OPT_INTERVAL,
	OPT_THRESHOLD, OPT_OVERLAP
};

CSimpleOpt::SOption SOptions[] =
{
		{ OPT_HELP, "-h", SO_NONE },
		{ OPT_HELP, "--help", SO_NONE },
		{ OPT_MODEL, "-m", SO_REQ_SEP },
		{ OPT_MODEL, "--model", SO_REQ_SEP },
		{ OPT_NAME, "-n", SO_REQ_SEP },
		{ OPT_NAME, "--name", SO_REQ_SEP },
		{ OPT_RESULTS, "-r", SO_REQ_SEP },
		{ OPT_RESULTS, "--results", SO_REQ_SEP },
		{ OPT_IMAGES, "-i", SO_REQ_SEP },
		{ OPT_IMAGES, "--images", SO_REQ_SEP },
		{ OPT_NB_NEG, "-z", SO_REQ_SEP },
		{ OPT_NB_NEG, "--nb-negatives", SO_REQ_SEP },
		{ OPT_PADDING, "-p", SO_REQ_SEP },
		{ OPT_PADDING, "--padding", SO_REQ_SEP },
		{ OPT_INTERVAL, "-e", SO_REQ_SEP },
		{ OPT_INTERVAL, "--interval", SO_REQ_SEP },
		{ OPT_THRESHOLD, "-t", SO_REQ_SEP },
		{ OPT_THRESHOLD, "--threshold", SO_REQ_SEP },
		{ OPT_OVERLAP, "-v", SO_REQ_SEP },
		{ OPT_OVERLAP, "--overlap", SO_REQ_SEP },
		SO_END_OF_OPTIONS
};

void showUsage()
{
	cout << "Usage: test [options] image.jpg, or\n       test [options] image_set.txt\n\n"
			"Options:\n"
			"  -h,--help               Display this information\n"
			"  -m,--model <file>       Read the input model from <file> (default \"model.txt\")\n"
			"  -n,--name <arg>         Name of the object to detect (default \"person\")\n"
			"  -r,--results <file>     Write the detection results to <file> (default none)\n"
			"  -i,--images <folder>    Draw the detections to <folder> (default none)\n"
			"  -z,--nb-negatives <arg> Maximum number of negative images to consider (default all)\n"
			"  -p,--padding <arg>      Amount of zero padding in HOG cells (default 12)\n"
			"  -e,--interval <arg>     Number of levels per octave in the HOG pyramid (default 10)\n"
			"  -t,--threshold <arg>    Minimum detection threshold (default -10)\n"
			"  -v,--overlap <arg>      Minimum overlap in non maxima suppression (default 0.5)"
			<< endl;
}

void draw(JPEGImage & image, const FFLD::Rectangle & rect, uint8_t r, uint8_t g, uint8_t b,
		int linewidth)
{
	if (image.empty() || rect.empty() || (image.depth() < 3))
		return;

	const int width = image.width();
	const int height = image.height();
	const int depth = image.depth();
	uint8_t * bits = image.bits();

	// Draw 2 horizontal lines
	const int top = min(max(rect.top(), 1), height - linewidth - 1);
	const int bottom = min(max(rect.bottom(), 1), height - linewidth - 1);

	for (int x = max(rect.left() - 1, 0); x <= min(rect.right() + linewidth, width - 1); ++x) {
		if ((x != max(rect.left() - 1, 0)) && (x != min(rect.right() + linewidth, width - 1))) {
			for (int i = 0; i < linewidth; ++i) {
				bits[((top + i) * width + x) * depth    ] = r;
				bits[((top + i) * width + x) * depth + 1] = g;
				bits[((top + i) * width + x) * depth + 2] = b;
				bits[((bottom + i) * width + x) * depth    ] = r;
				bits[((bottom + i) * width + x) * depth + 1] = g;
				bits[((bottom + i) * width + x) * depth + 2] = b;
			}
		}

		// Draw a white line below and above the line
		if ((bits[((top - 1) * width + x) * depth    ] != 255) &&
				(bits[((top - 1) * width + x) * depth + 1] != 255) &&
				(bits[((top - 1) * width + x) * depth + 2] != 255))
			for (int i = 0; i < 3; ++i)
				bits[((top - 1) * width + x) * depth + i] = 255;

		if ((bits[((top + linewidth) * width + x) * depth    ] != 255) &&
				(bits[((top + linewidth) * width + x) * depth + 1] != 255) &&
				(bits[((top + linewidth) * width + x) * depth + 2] != 255))
			for (int i = 0; i < 3; ++i)
				bits[((top + linewidth) * width + x) * depth + i] = 255;

		if ((bits[((bottom - 1) * width + x) * depth    ] != 255) &&
				(bits[((bottom - 1) * width + x) * depth + 1] != 255) &&
				(bits[((bottom - 1) * width + x) * depth + 2] != 255))
			for (int i = 0; i < 3; ++i)
				bits[((bottom - 1) * width + x) * depth + i] = 255;

		if ((bits[((bottom + linewidth) * width + x) * depth    ] != 255) &&
				(bits[((bottom + linewidth) * width + x) * depth + 1] != 255) &&
				(bits[((bottom + linewidth) * width + x) * depth + 2] != 255))
			for (int i = 0; i < 3; ++i)
				bits[((bottom + linewidth) * width + x) * depth + i] = 255;
	}

	// Draw 2 vertical lines
	const int left = min(max(rect.left(), 1), width - linewidth - 1);
	const int right = min(max(rect.right(), 1), width - linewidth - 1);

	for (int y = max(rect.top() - 1, 0); y <= min(rect.bottom() + linewidth, height - 1); ++y) {
		if ((y != max(rect.top() - 1, 0)) && (y != min(rect.bottom() + linewidth, height - 1))) {
			for (int i = 0; i < linewidth; ++i) {
				bits[(y * width + left + i) * depth    ] = r;
				bits[(y * width + left + i) * depth + 1] = g;
				bits[(y * width + left + i) * depth + 2] = b;
				bits[(y * width + right + i) * depth    ] = r;
				bits[(y * width + right + i) * depth + 1] = g;
				bits[(y * width + right + i) * depth + 2] = b;
			}
		}

		// Draw a white line left and right the line
		if ((bits[(y * width + left - 1) * depth    ] != 255) &&
				(bits[(y * width + left - 1) * depth + 1] != 255) &&
				(bits[(y * width + left - 1) * depth + 2] != 255))
			for (int i = 0; i < 3; ++i)
				bits[(y * width + left - 1) * depth + i] = 255;

		if ((bits[(y * width + left + linewidth) * depth    ] != 255) &&
				(bits[(y * width + left + linewidth) * depth + 1] != 255) &&
				(bits[(y * width + left + linewidth) * depth + 2] != 255))
			for (int i = 0; i < 3; ++i)
				bits[(y * width + left + linewidth) * depth + i] = 255;

		if ((bits[(y * width + right - 1) * depth    ] != 255) &&
				(bits[(y * width + right - 1) * depth + 1] != 255) &&
				(bits[(y * width + right - 1) * depth + 2] != 255))
			for (int i = 0; i < 3; ++i)
				bits[(y * width + right - 1) * depth + i] = 255;

		if ((bits[(y * width + right + linewidth) * depth    ] != 255) &&
				(bits[(y * width + right + linewidth) * depth + 1] != 255) &&
				(bits[(y * width + right + linewidth) * depth + 2] != 255))
			for (int i = 0; i < 3; ++i)
				bits[(y * width + right + linewidth) * depth + i] = 255;
	}
}

void detect(const Mixture & mixture, int width, int height, const HOGPyramid & pyramid,
		double threshold, double overlap, const string image, ostream & out,
		const string & images, vector<Detection> & detections)
{
	// Compute the scores
	vector<HOGPyramid::Matrix> scores;
	vector<Mixture::Indices> argmaxes;
	vector<vector<vector<Model::Positions> > > positions;

	if (!images.empty())
		mixture.convolve(pyramid, scores, argmaxes, &positions);
	else
		mixture.convolve(pyramid, scores, argmaxes);

	// Cache the size of the models
	vector<pair<int, int> > sizes(mixture.models().size());

	for (int i = 0; i < sizes.size(); ++i)
		sizes[i] = mixture.models()[i].rootSize();

	// For each scale
	for (int i = pyramid.interval(); i < scores.size(); ++i) {
		// Scale = 8 / 2^(1 - i / interval)
		const double scale = pow(2.0, static_cast<double>(i) / pyramid.interval() + 2.0);

		const int rows = scores[i].rows();
		const int cols = scores[i].cols();

		for (int y = 0; y < rows; ++y) {
			for (int x = 0; x < cols; ++x) {
				const float score = scores[i](y, x);

				if (score > threshold) {
					if (((y == 0) || (x == 0) || (score > scores[i](y - 1, x - 1))) &&
							((y == 0) || (score > scores[i](y - 1, x))) &&
							((y == 0) || (x == cols - 1) || (score > scores[i](y - 1, x + 1))) &&
							((x == 0) || (score > scores[i](y, x - 1))) &&
							((x == cols - 1) || (score > scores[i](y, x + 1))) &&
							((y == rows - 1) || (x == 0) || (score > scores[i](y + 1, x - 1))) &&
							((y == rows - 1) || (score > scores[i](y + 1, x))) &&
							((y == rows - 1) || (x == cols - 1) || (score > scores[i](y + 1, x + 1)))) {
						FFLD::Rectangle bndbox((x - pyramid.padx()) * scale + 0.5,
								(y - pyramid.pady()) * scale + 0.5,
								sizes[argmaxes[i](y, x)].second * scale + 0.5,
								sizes[argmaxes[i](y, x)].first * scale + 0.5);

						// Truncate the object
						bndbox.setX(max(bndbox.x(), 0));
						bndbox.setY(max(bndbox.y(), 0));
						bndbox.setWidth(min(bndbox.width(), width - bndbox.x()));
						bndbox.setHeight(min(bndbox.height(), height - bndbox.y()));

						if (!bndbox.empty())
							detections.push_back(Detection(score, i, x, y, bndbox));
					}
				}
			}
		}
	}

	// Non maxima suppression
	sort(detections.begin(), detections.end());

	for (int i = 1; i < detections.size(); ++i)
		detections.resize(remove_if(detections.begin() + i, detections.end(),
				Intersector(detections[i - 1], overlap, true)) -
				detections.begin());

	// Print the detection
	const size_t lastDot = image.find_last_of('.');

	string id = image.substr(0, lastDot);

	const size_t lastSlash = id.find_last_of("/\\");

	if (lastSlash != string::npos)
		id = id.substr(lastSlash + 1);

	if (out) {
#pragma omp critical
		for (int i = 0; i < detections.size(); ++i)
			out << id << ' ' << detections[i].score << ' ' << (detections[i].left() + 1) << ' '
			<< (detections[i].top() + 1) << ' ' << (detections[i].right() + 1) << ' '
			<< (detections[i].bottom() + 1) << endl;
	}

	if (!images.empty()) {
		JPEGImage im(image);

		for (int j = 0; j < detections.size(); ++j) {
			// The position of the root one octave below
			const int argmax = argmaxes[detections[j].l](detections[j].y, detections[j].x);
			const int x2 = detections[j].x * 2 - pyramid.padx();
			const int y2 = detections[j].y * 2 - pyramid.pady();
			const int l = detections[j].l - pyramid.interval();

			// Scale = 8 / 2^(1 - j / interval)
			const double scale = pow(2.0, static_cast<double>(l) / pyramid.interval() + 2.0);

			for (int k = 0; k < positions[argmax].size(); ++k) {
				const FFLD::Rectangle bndbox((positions[argmax][k][l](y2, x2)(0) - pyramid.padx()) *
						scale + 0.5,
						(positions[argmax][k][l](y2, x2)(1) - pyramid.pady()) *
						scale + 0.5,
						mixture.models()[argmax].partSize().second * scale + 0.5,
						mixture.models()[argmax].partSize().second * scale + 0.5);

				draw(im, bndbox, 0, 0, 255, 2);
			}

			// Draw the root last
			draw(im, detections[j], 255, 0, 0, 2);
		}

		im.save(images + '/' + id + ".jpg");
	}
}




