#include <algorithm>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <vector>
#include <time.h>
#include <stdlib.h>
#include <string>

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/contrib/contrib.hpp"

#include <ros/ros.h>
#include <ros/package.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "reconfig_common_msgs/ObjectDetectPx.h"		// srv
#include "reconfig_common_msgs/DetectResult2dPx.h"		// msg
#include "reconfig_common_msgs/ObjClass.h"			// msg
#include "reconfig_common_msgs/Point2d.h"			// msg
#include "reconfig_common_msgs/Rectangle2d.h"			// msg

#include <boost/interprocess/sync/interprocess_semaphore.hpp>
#include <boost/atomic.hpp>

#include <SimpleOpt.h>
#include <Intersector.h>
#include <Mixture.h>
#include <Scene.h>
#include <HOGPyramid.h>
#include <Patchwork.h>
#include <sys/time.h>


#define BILLION  1E9

using namespace std;
using namespace cv;
using namespace ros;

struct Detection : public FFLD::Rectangle
{
	FFLD::HOGPyramid::Scalar score;
	int l;
	int x;
	int y;

	Detection() : score(0), l(0), x(0), y(0)
	{
	}

	Detection(FFLD::HOGPyramid::Scalar score, int l, int x, int y, FFLD::Rectangle bndbox) :
		FFLD::Rectangle(bndbox), score(score), l(l), x(x), y(y)
	{
	}

	bool operator<(const Detection & detection) const
	{
		return score > detection.score;
	}
};

string dtostr(double number);

struct Model_struct
{
	string path;
	double threshold;
	string request_string;

	Model_struct() : path(""), threshold(0), request_string("")
	{
	}

	Model_struct(string path, double threshold, string request_string) : path(path), threshold(threshold), request_string(request_string)
	{
	}

	string toString()
	{
		string ret = "model path: ";
		ret.append(path);
		ret.append("\nthreshold: ");
		ret.append(dtostr(threshold));
		ret.append("\nrequest_key: ");
		ret.append("request_string");
		return ret;
	}
};

// Global variable
cv::Mat g_matImg;
vector<Model_struct>  g_vXMLFiles;
boost::atomic<bool> g_requested(false);
boost::interprocess::interprocess_semaphore detectingSema (0);
reconfig_common_msgs::ObjectDetectPx::Response g_response;
void callbackDetect( const sensor_msgs::ImageConstPtr& msg );
void readConfigFile( string strFilename );
bool serviceDetect( reconfig_common_msgs::ObjectDetectPx::Request &request, reconfig_common_msgs::ObjectDetectPx::Response &response );
void detect(const FFLD::Mixture & mixture, int width, int height, const FFLD::HOGPyramid & pyramid,  double threshold, double overlap, const string image, ostream & out, const string & images, vector<Detection> & detections);
int stop();


string g_requiredClass;

string strPackagePath;


int main(int argc, char** argv)
{
	ros::init(argc, argv, "dpm_detector_srv");
	strPackagePath = ros::package::getPath("ffld");

	string strCfgFilename = strPackagePath + "/data/test.cfg";
	readConfigFile( strCfgFilename );

	std::cout << "PATH: " << strCfgFilename <<  std::endl;

	ros::NodeHandle nodeHandle;
	image_transport::ImageTransport 	rosImageTrans( nodeHandle );
	image_transport::Subscriber		rosImageSub;

	cv::namedWindow("image");
	rosImageSub = rosImageTrans.subscribe( "/py_camera/image_raw", 1, callbackDetect );

	// offer the service
	ServiceServer service = nodeHandle.advertiseService("srv_detect_obj_px", serviceDetect);

	std::cout << "dpm detector is running" << std::endl;

	ros::spin();

	return 0;
}

void readConfigFile( string strFilename )
{
	string line;
	ifstream file( strFilename.c_str() );

	if( file.is_open() )
	{
		int cnt = 0;
		while( getline( file, line ) )
		{
			string str_id = line.substr(0, line.find(' '));
			line = line.substr(line.find(' ')+1);
			char* nu = NULL;
			g_vXMLFiles.push_back(Model_struct( 
				strPackagePath + line.substr(0, line.find(' ')) , 
				strtod(line.substr(line.find(' ')+1).c_str(), &nu), 
				str_id));
std::cout << "readConfigFile: " << g_vXMLFiles.at(cnt).toString() << std::endl;
			cnt++;
		}
		std::cout << "config file read with " << cnt << " models." << std::endl;
	}
	else
	{
		ROS_ERROR("Unable to open config file.");
	}
}

bool serviceDetect( reconfig_common_msgs::ObjectDetectPx::Request &request, reconfig_common_msgs::ObjectDetectPx::Response &response )
{
	std::cout << "yeah, got a request" << std::endl;
	/*    g_requiredClasses.clear();
    for(int i = 0; i < request.ObjClass.size(); i++){
        g_requiredClasses.push_back(request.ObjClass[i]);
    }*/
	g_requiredClass = request.ObjClass;
std::cout << "stop1" << std::endl;
	g_requested.store(true, boost::memory_order_relaxed);
	while(g_requested.load(boost::memory_order_relaxed)){
//std::cout << "not stuck in the semaphore, but in the spinOnce-loop" << std::endl;
		spinOnce();
        }
std::cout << "stop3" << std::endl;
	//detectingSema.wait();
	std::cout << "requested object class: " << request.ObjClass << std::endl;
	response = g_response;
	response.id = request.id;
	return true;
}

void callbackDetect(const sensor_msgs::ImageConstPtr& msg)
{
//	std::cout << "received picture " << rand() << std::endl;
//	if ((char) cv::waitKey(30) != 27){
		// only detect something if we are ordered to to so
		if(!g_requested.load(boost::memory_order_relaxed)) return;
//	}
	std::cout << "start detecting process " << std::endl;
	g_response.detected_objects.clear();
	// copy the picture from the sensor message to a usable openCV type
	cv_bridge::CvImagePtr cv_ptr;

	try
	{
		cv_ptr = cv_bridge::toCvCopy( msg, sensor_msgs::image_encodings::TYPE_8UC3 );
	}
	catch (cv_bridge::Exception& e)
	{
		ROS_ERROR("cv_bridge exception: %s", e.what());
		return;
	}


	// let the fun begin
	using namespace FFLD;
	using namespace std;

	int padding = 12;
	double overlap = 0.3;
	int interval = 10;
	const string file(strPackagePath + "/data/temp/input.jpg");
	const string images(strPackagePath + "/data/output");
	const string resultsFile(strPackagePath + "/data/output/results.txt");

	int modelNumber;
	for(int i = 0; i < g_vXMLFiles.size(); i++){
		if(g_requiredClass.compare(g_vXMLFiles.at(i).request_string) == 0){
			modelNumber = i;
std::cout << "requested model = " << g_vXMLFiles.at(i).toString() << std::endl;
		}
	}
		

	// for time measurements
	struct timespec start_t,finish_t;
	double time_taken, total_time = 0.0;
	clock_gettime(CLOCK_REALTIME,&start_t);

	g_matImg = cv_ptr->image.clone();
	bool success = imwrite(file, g_matImg);
	if (success){
		std::cout << "image written to temp" << std::endl;
	}
	else{
		std::cout << "check if folder " << strPackagePath << "/data/temp exists!" << std::endl;
		return;
	}

	// read image file into the right format
	JPEGImage image(file);
	if (image.empty()) {
		std::cout << "Temp file not found or invalid" << std::endl;
		return;
	}

	// Try to open the model
	ifstream in(g_vXMLFiles.at(modelNumber).path.c_str(), ios::binary);

	if (!in.is_open()) {
		std::cout << "Invalid model file " << g_vXMLFiles.at(modelNumber).path << std::endl;
		return;
	}

	Mixture mixture;
	in >> mixture;

	if (mixture.empty()) {
		std::cout << "Invalid model file " << g_vXMLFiles.at(modelNumber).path << std::endl;
		return;
	}

	// Try to open the results
	ofstream out;
	out.open(resultsFile.c_str(), ios::binary);

	if (!out.is_open()) {
		std::cout << "Invalid results file " << resultsFile << std::endl;
		return;
	}

	// Compute the HOG features
	start();

	HOGPyramid pyramid(image, padding, padding, interval);

	if (pyramid.empty()) {
		std::cout << "Invalid image " << strPackagePath << "/data/temp/input.jpg" << std::endl;
		return;
	}

	std::cout << "Computed HOG features in " << stop() << " ms" << std::endl;

	// Initialize the Patchwork class
	start();

	if (!Patchwork::Init((pyramid.levels()[0].rows() - padding + 15) & ~15, (pyramid.levels()[0].cols() - padding + 15) & ~15)) {
		std::cout << "Could not initialize the Patchwork class" << std::endl;
		return;
	}

	std::cout << "Initialized FFTW in " << stop() << " ms" << std::endl;

	start();

	mixture.cacheFilters();

	std::cout << "Transformed the filters in " << stop() << " ms" << std::endl;

	// Compute the detections
	start();
	vector<Detection> detections;



	detect(mixture, image.width(), image.height(), pyramid, g_vXMLFiles.at(modelNumber).threshold, overlap, file, out, images, detections);

	clock_gettime(CLOCK_REALTIME,&finish_t);
	time_taken = (finish_t.tv_sec - start_t.tv_sec) + (finish_t.tv_nsec - start_t.tv_nsec)/BILLION;
	cout<<"Detection took "<<time_taken<<" seconds."<<endl;

	std::cout << "Computed the convolutions and distance transforms in " << stop() << " ms" << std::endl;


	for(size_t i = 0; i < detections.size(); i++ )
	{
		reconfig_common_msgs::DetectResult2dPx res;
		res.ObjClass = g_requiredClass;
		res.detScore = (float)detections.at(i).score;
		res.bBox.upperLeft.x = detections.at(i).left();
		res.bBox.upperLeft.y = detections.at(i).top();
		res.bBox.lowerRight.x = detections.at(i).right();
		res.bBox.lowerRight.y = detections.at(i).bottom();
		res.centroid.x = (res.bBox.upperLeft.x + res.bBox.lowerRight.x)/2;
		res.centroid.y = (res.bBox.upperLeft.y + res.bBox.lowerRight.y)/2;

		g_response.detected_objects.push_back( res );

		ROS_INFO_STREAM( "ClassName:" << g_response.detected_objects[i].ObjClass << "\nScore:" << res.detScore << " Centre(x,y):" << res.centroid.x << ", " << res.centroid.y << " bBox.upperLeft.x:" << res.bBox.upperLeft.x << " bBox.upperLeft.y:" << res.bBox.upperLeft.y << " bBox.lowerRight.x:" << res.bBox.lowerRight.x << " bBox.lowerRight.y:" << res.bBox.lowerRight.y);
	}

std::cout << "setting g_requested to false now" << std::endl;
	g_requested.store(false, boost::memory_order_relaxed);
std::cout << "I have set g_requested to false now" << std::endl;
	//detectingSema.post();

}


timeval Start, Stop;


inline void start()
{
	gettimeofday(&Start, 0);
}

int stop()
{
	gettimeofday(&Stop, 0);

	timeval duration;
	timersub(&Stop, &Start, &duration);

	return duration.tv_sec * 1000 + (duration.tv_usec + 500) / 1000;
}

using namespace FFLD;
using namespace std;


// SimpleOpt array of valid options
enum
{
	OPT_HELP, OPT_MODEL, OPT_NAME, OPT_RESULTS, OPT_IMAGES, OPT_NB_NEG, OPT_PADDING, OPT_INTERVAL,
	OPT_THRESHOLD, OPT_OVERLAP
};

CSimpleOpt::SOption SOptions[] =
{
		{ OPT_HELP, "-h", SO_NONE },
		{ OPT_HELP, "--help", SO_NONE },
		{ OPT_MODEL, "-m", SO_REQ_SEP },
		{ OPT_MODEL, "--model", SO_REQ_SEP },
		{ OPT_NAME, "-n", SO_REQ_SEP },
		{ OPT_NAME, "--name", SO_REQ_SEP },
		{ OPT_RESULTS, "-r", SO_REQ_SEP },
		{ OPT_RESULTS, "--results", SO_REQ_SEP },
		{ OPT_IMAGES, "-i", SO_REQ_SEP },
		{ OPT_IMAGES, "--images", SO_REQ_SEP },
		{ OPT_NB_NEG, "-z", SO_REQ_SEP },
		{ OPT_NB_NEG, "--nb-negatives", SO_REQ_SEP },
		{ OPT_PADDING, "-p", SO_REQ_SEP },
		{ OPT_PADDING, "--padding", SO_REQ_SEP },
		{ OPT_INTERVAL, "-e", SO_REQ_SEP },
		{ OPT_INTERVAL, "--interval", SO_REQ_SEP },
		{ OPT_THRESHOLD, "-t", SO_REQ_SEP },
		{ OPT_THRESHOLD, "--threshold", SO_REQ_SEP },
		{ OPT_OVERLAP, "-v", SO_REQ_SEP },
		{ OPT_OVERLAP, "--overlap", SO_REQ_SEP },
		SO_END_OF_OPTIONS
};

void showUsage()
{
	cout << "Usage: test [options] image.jpg, or\n       test [options] image_set.txt\n\n"
			"Options:\n"
			"  -h,--help               Display this information\n"
			"  -m,--model <file>       Read the input model from <file> (default \"model.txt\")\n"
			"  -n,--name <arg>         Name of the object to detect (default \"person\")\n"
			"  -r,--results <file>     Write the detection results to <file> (default none)\n"
			"  -i,--images <folder>    Draw the detections to <folder> (default none)\n"
			"  -z,--nb-negatives <arg> Maximum number of negative images to consider (default all)\n"
			"  -p,--padding <arg>      Amount of zero padding in HOG cells (default 12)\n"
			"  -e,--interval <arg>     Number of levels per octave in the HOG pyramid (default 10)\n"
			"  -t,--threshold <arg>    Minimum detection threshold (default -10)\n"
			"  -v,--overlap <arg>      Minimum overlap in non maxima suppression (default 0.5)"
			<< endl;
}

void draw(JPEGImage & image, const FFLD::Rectangle & rect, uint8_t r, uint8_t g, uint8_t b,
		int linewidth)
{
	if (image.empty() || rect.empty() || (image.depth() < 3))
		return;

	const int width = image.width();
	const int height = image.height();
	const int depth = image.depth();
	uint8_t * bits = image.bits();

	// Draw 2 horizontal lines
	const int top = min(max(rect.top(), 1), height - linewidth - 1);
	const int bottom = min(max(rect.bottom(), 1), height - linewidth - 1);

	for (int x = max(rect.left() - 1, 0); x <= min(rect.right() + linewidth, width - 1); ++x) {
		if ((x != max(rect.left() - 1, 0)) && (x != min(rect.right() + linewidth, width - 1))) {
			for (int i = 0; i < linewidth; ++i) {
				bits[((top + i) * width + x) * depth    ] = r;
				bits[((top + i) * width + x) * depth + 1] = g;
				bits[((top + i) * width + x) * depth + 2] = b;
				bits[((bottom + i) * width + x) * depth    ] = r;
				bits[((bottom + i) * width + x) * depth + 1] = g;
				bits[((bottom + i) * width + x) * depth + 2] = b;
			}
		}

		// Draw a white line below and above the line
		if ((bits[((top - 1) * width + x) * depth    ] != 255) &&
				(bits[((top - 1) * width + x) * depth + 1] != 255) &&
				(bits[((top - 1) * width + x) * depth + 2] != 255))
			for (int i = 0; i < 3; ++i)
				bits[((top - 1) * width + x) * depth + i] = 255;

		if ((bits[((top + linewidth) * width + x) * depth    ] != 255) &&
				(bits[((top + linewidth) * width + x) * depth + 1] != 255) &&
				(bits[((top + linewidth) * width + x) * depth + 2] != 255))
			for (int i = 0; i < 3; ++i)
				bits[((top + linewidth) * width + x) * depth + i] = 255;

		if ((bits[((bottom - 1) * width + x) * depth    ] != 255) &&
				(bits[((bottom - 1) * width + x) * depth + 1] != 255) &&
				(bits[((bottom - 1) * width + x) * depth + 2] != 255))
			for (int i = 0; i < 3; ++i)
				bits[((bottom - 1) * width + x) * depth + i] = 255;

		if ((bits[((bottom + linewidth) * width + x) * depth    ] != 255) &&
				(bits[((bottom + linewidth) * width + x) * depth + 1] != 255) &&
				(bits[((bottom + linewidth) * width + x) * depth + 2] != 255))
			for (int i = 0; i < 3; ++i)
				bits[((bottom + linewidth) * width + x) * depth + i] = 255;
	}

	// Draw 2 vertical lines
	const int left = min(max(rect.left(), 1), width - linewidth - 1);
	const int right = min(max(rect.right(), 1), width - linewidth - 1);

	for (int y = max(rect.top() - 1, 0); y <= min(rect.bottom() + linewidth, height - 1); ++y) {
		if ((y != max(rect.top() - 1, 0)) && (y != min(rect.bottom() + linewidth, height - 1))) {
			for (int i = 0; i < linewidth; ++i) {
				bits[(y * width + left + i) * depth    ] = r;
				bits[(y * width + left + i) * depth + 1] = g;
				bits[(y * width + left + i) * depth + 2] = b;
				bits[(y * width + right + i) * depth    ] = r;
				bits[(y * width + right + i) * depth + 1] = g;
				bits[(y * width + right + i) * depth + 2] = b;
			}
		}

		// Draw a white line left and right the line
		if ((bits[(y * width + left - 1) * depth    ] != 255) &&
				(bits[(y * width + left - 1) * depth + 1] != 255) &&
				(bits[(y * width + left - 1) * depth + 2] != 255))
			for (int i = 0; i < 3; ++i)
				bits[(y * width + left - 1) * depth + i] = 255;

		if ((bits[(y * width + left + linewidth) * depth    ] != 255) &&
				(bits[(y * width + left + linewidth) * depth + 1] != 255) &&
				(bits[(y * width + left + linewidth) * depth + 2] != 255))
			for (int i = 0; i < 3; ++i)
				bits[(y * width + left + linewidth) * depth + i] = 255;

		if ((bits[(y * width + right - 1) * depth    ] != 255) &&
				(bits[(y * width + right - 1) * depth + 1] != 255) &&
				(bits[(y * width + right - 1) * depth + 2] != 255))
			for (int i = 0; i < 3; ++i)
				bits[(y * width + right - 1) * depth + i] = 255;

		if ((bits[(y * width + right + linewidth) * depth    ] != 255) &&
				(bits[(y * width + right + linewidth) * depth + 1] != 255) &&
				(bits[(y * width + right + linewidth) * depth + 2] != 255))
			for (int i = 0; i < 3; ++i)
				bits[(y * width + right + linewidth) * depth + i] = 255;
	}
}

void detect(const Mixture & mixture, int width, int height, const HOGPyramid & pyramid,
		double threshold, double overlap, const string image, ostream & out,
		const string & images, vector<Detection> & detections)
{
	// Compute the scores
	vector<HOGPyramid::Matrix> scores;
	vector<Mixture::Indices> argmaxes;
	vector<vector<vector<Model::Positions> > > positions;

	if (!images.empty())
		mixture.convolve(pyramid, scores, argmaxes, &positions);
	else
		mixture.convolve(pyramid, scores, argmaxes);

	// Cache the size of the models
	vector<pair<int, int> > sizes(mixture.models().size());

	for (int i = 0; i < sizes.size(); ++i)
		sizes[i] = mixture.models()[i].rootSize();

	// For each scale
	for (int i = pyramid.interval(); i < scores.size(); ++i) {
		// Scale = 8 / 2^(1 - i / interval)
		const double scale = pow(2.0, static_cast<double>(i) / pyramid.interval() + 2.0);

		const int rows = scores[i].rows();
		const int cols = scores[i].cols();

		for (int y = 0; y < rows; ++y) {
			for (int x = 0; x < cols; ++x) {
				const float score = scores[i](y, x);

				if (score > threshold) {
					if (((y == 0) || (x == 0) || (score > scores[i](y - 1, x - 1))) &&
							((y == 0) || (score > scores[i](y - 1, x))) &&
							((y == 0) || (x == cols - 1) || (score > scores[i](y - 1, x + 1))) &&
							((x == 0) || (score > scores[i](y, x - 1))) &&
							((x == cols - 1) || (score > scores[i](y, x + 1))) &&
							((y == rows - 1) || (x == 0) || (score > scores[i](y + 1, x - 1))) &&
							((y == rows - 1) || (score > scores[i](y + 1, x))) &&
							((y == rows - 1) || (x == cols - 1) || (score > scores[i](y + 1, x + 1)))) {
						FFLD::Rectangle bndbox((x - pyramid.padx()) * scale + 0.5,
								(y - pyramid.pady()) * scale + 0.5,
								sizes[argmaxes[i](y, x)].second * scale + 0.5,
								sizes[argmaxes[i](y, x)].first * scale + 0.5);

						// Truncate the object
						bndbox.setX(max(bndbox.x(), 0));
						bndbox.setY(max(bndbox.y(), 0));
						bndbox.setWidth(min(bndbox.width(), width - bndbox.x()));
						bndbox.setHeight(min(bndbox.height(), height - bndbox.y()));

						if (!bndbox.empty())
							detections.push_back(Detection(score, i, x, y, bndbox));
					}
				}
			}
		}
	}

	// Non maxima suppression
	sort(detections.begin(), detections.end());

	for (int i = 1; i < detections.size(); ++i)
		detections.resize(remove_if(detections.begin() + i, detections.end(),
				Intersector(detections[i - 1], overlap, true)) -
				detections.begin());

	// Print the detection
	const size_t lastDot = image.find_last_of('.');

	string id = image.substr(0, lastDot);

	const size_t lastSlash = id.find_last_of("/\\");

	if (lastSlash != string::npos)
		id = id.substr(lastSlash + 1);

	if (out) {
#pragma omp critical
		for (int i = 0; i < detections.size(); ++i)
			out << id << ' ' << detections[i].score << ' ' << (detections[i].left() + 1) << ' '
			<< (detections[i].top() + 1) << ' ' << (detections[i].right() + 1) << ' '
			<< (detections[i].bottom() + 1) << endl;
	}

	if (!images.empty()) {
		JPEGImage im(image);

		for (int j = 0; j < detections.size(); ++j) {
			// The position of the root one octave below
			const int argmax = argmaxes[detections[j].l](detections[j].y, detections[j].x);
			const int x2 = detections[j].x * 2 - pyramid.padx();
			const int y2 = detections[j].y * 2 - pyramid.pady();
			const int l = detections[j].l - pyramid.interval();

			// Scale = 8 / 2^(1 - j / interval)
			const double scale = pow(2.0, static_cast<double>(l) / pyramid.interval() + 2.0);

			for (int k = 0; k < positions[argmax].size(); ++k) {
				const FFLD::Rectangle bndbox((positions[argmax][k][l](y2, x2)(0) - pyramid.padx()) *
						scale + 0.5,
						(positions[argmax][k][l](y2, x2)(1) - pyramid.pady()) *
						scale + 0.5,
						mixture.models()[argmax].partSize().second * scale + 0.5,
						mixture.models()[argmax].partSize().second * scale + 0.5);

				draw(im, bndbox, 0, 0, 255, 2);
			}

			// Draw the root last
			draw(im, detections[j], 255, 0, 0, 2);
		}

		im.save(images + '/' + id + ".jpg");
	}
}

string dtostr(double number){
	std::ostringstream strs;
	strs << number;
	std::string str = strs.str();
	return str;
}


