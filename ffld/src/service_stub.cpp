#include "ros/ros.h"
#include <string>

#include "reconfig_common_msgs/ObjectDetectPx.h"                // srv
#include "reconfig_common_msgs/DetectResult2dPx.h"              // msg
#include "reconfig_common_msgs/ObjClass.h"                      // msg
#include "reconfig_common_msgs/Point2d.h"                       // msg
#include "reconfig_common_msgs/Rectangle2d.h"                   // msg

using namespace ros;


int main(int argc, char **argv)
{
  ros::init(argc, argv, "service_stub");
  if (argc != 1)
  {
    ROS_INFO("usage: no parameters");
    return 1;
  }

  ros::NodeHandle n;
  ros::ServiceClient client = n.serviceClient<reconfig_common_msgs::ObjectDetectPx>("srv_detect_obj_px");
  reconfig_common_msgs::ObjectDetectPx srv;
  srv.request.id = 1;
  srv.request.ObjClass = "grab_object";//"toy-car"; //"ball"
  if (client.call(srv))
  {
    ROS_INFO("Detected objects: %i", (int)srv.response.detected_objects.size());
    for(size_t i = 0; i < (int)srv.response.detected_objects.size(); i++ )
    {
        reconfig_common_msgs::DetectResult2dPx res = srv.response.detected_objects[i];

        ROS_INFO_STREAM( "ClassName: " << res.ObjClass << "\nScore: " << res.detScore << " Centre(x,y): " << res.centroid.x << ", " << res.centroid.y << " Width: " << res.bBox.lowerRight.x - res.bBox.upperLeft.x << " Height: " << res.bBox.lowerRight.y - res.bBox.upperLeft.y);

        }

  }
  else
  {
    ROS_ERROR("Failed to call service srv_detect_obj_px");
    return 1;
  }

  return 0;
}
