# Install script for directory: /home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/src/SparseCore

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Devel")
  list(APPEND CPACK_ABSOLUTE_DESTINATION_FILES
   "/usr/local/include/eigen3/Eigen/src/SparseCore/SparseFuzzy.h;/usr/local/include/eigen3/Eigen/src/SparseCore/SparseCwiseBinaryOp.h;/usr/local/include/eigen3/Eigen/src/SparseCore/SparseProduct.h;/usr/local/include/eigen3/Eigen/src/SparseCore/SparseMatrixBase.h;/usr/local/include/eigen3/Eigen/src/SparseCore/TriangularSolver.h;/usr/local/include/eigen3/Eigen/src/SparseCore/AmbiVector.h;/usr/local/include/eigen3/Eigen/src/SparseCore/SparseView.h;/usr/local/include/eigen3/Eigen/src/SparseCore/CompressedStorage.h;/usr/local/include/eigen3/Eigen/src/SparseCore/SparseTriangularView.h;/usr/local/include/eigen3/Eigen/src/SparseCore/SparseBlock.h;/usr/local/include/eigen3/Eigen/src/SparseCore/SparseSelfAdjointView.h;/usr/local/include/eigen3/Eigen/src/SparseCore/SparseDiagonalProduct.h;/usr/local/include/eigen3/Eigen/src/SparseCore/MappedSparseMatrix.h;/usr/local/include/eigen3/Eigen/src/SparseCore/SparseVector.h;/usr/local/include/eigen3/Eigen/src/SparseCore/SparseRedux.h;/usr/local/include/eigen3/Eigen/src/SparseCore/ConservativeSparseSparseProduct.h;/usr/local/include/eigen3/Eigen/src/SparseCore/SparseDot.h;/usr/local/include/eigen3/Eigen/src/SparseCore/SparseCwiseUnaryOp.h;/usr/local/include/eigen3/Eigen/src/SparseCore/SparseAssign.h;/usr/local/include/eigen3/Eigen/src/SparseCore/SparsePermutation.h;/usr/local/include/eigen3/Eigen/src/SparseCore/CoreIterators.h;/usr/local/include/eigen3/Eigen/src/SparseCore/SparseTranspose.h;/usr/local/include/eigen3/Eigen/src/SparseCore/SparseDenseProduct.h;/usr/local/include/eigen3/Eigen/src/SparseCore/SparseMatrix.h;/usr/local/include/eigen3/Eigen/src/SparseCore/SparseSparseProductWithPruning.h;/usr/local/include/eigen3/Eigen/src/SparseCore/SparseUtil.h")
FILE(INSTALL DESTINATION "/usr/local/include/eigen3/Eigen/src/SparseCore" TYPE FILE FILES
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/src/SparseCore/SparseFuzzy.h"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/src/SparseCore/SparseCwiseBinaryOp.h"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/src/SparseCore/SparseProduct.h"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/src/SparseCore/SparseMatrixBase.h"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/src/SparseCore/TriangularSolver.h"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/src/SparseCore/AmbiVector.h"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/src/SparseCore/SparseView.h"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/src/SparseCore/CompressedStorage.h"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/src/SparseCore/SparseTriangularView.h"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/src/SparseCore/SparseBlock.h"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/src/SparseCore/SparseSelfAdjointView.h"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/src/SparseCore/SparseDiagonalProduct.h"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/src/SparseCore/MappedSparseMatrix.h"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/src/SparseCore/SparseVector.h"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/src/SparseCore/SparseRedux.h"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/src/SparseCore/ConservativeSparseSparseProduct.h"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/src/SparseCore/SparseDot.h"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/src/SparseCore/SparseCwiseUnaryOp.h"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/src/SparseCore/SparseAssign.h"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/src/SparseCore/SparsePermutation.h"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/src/SparseCore/CoreIterators.h"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/src/SparseCore/SparseTranspose.h"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/src/SparseCore/SparseDenseProduct.h"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/src/SparseCore/SparseMatrix.h"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/src/SparseCore/SparseSparseProductWithPruning.h"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/src/SparseCore/SparseUtil.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Devel")

