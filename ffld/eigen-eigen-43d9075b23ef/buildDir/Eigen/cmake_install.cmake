# Install script for directory: /home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Devel")
  list(APPEND CPACK_ABSOLUTE_DESTINATION_FILES
   "/usr/local/include/eigen3/Eigen/QtAlignedMalloc;/usr/local/include/eigen3/Eigen/PardisoSupport;/usr/local/include/eigen3/Eigen/UmfPackSupport;/usr/local/include/eigen3/Eigen/Eigen2Support;/usr/local/include/eigen3/Eigen/Array;/usr/local/include/eigen3/Eigen/OrderingMethods;/usr/local/include/eigen3/Eigen/Sparse;/usr/local/include/eigen3/Eigen/CholmodSupport;/usr/local/include/eigen3/Eigen/SparseCholesky;/usr/local/include/eigen3/Eigen/StdDeque;/usr/local/include/eigen3/Eigen/Jacobi;/usr/local/include/eigen3/Eigen/Core;/usr/local/include/eigen3/Eigen/PaStiXSupport;/usr/local/include/eigen3/Eigen/SparseCore;/usr/local/include/eigen3/Eigen/Cholesky;/usr/local/include/eigen3/Eigen/SuperLUSupport;/usr/local/include/eigen3/Eigen/QR;/usr/local/include/eigen3/Eigen/Eigenvalues;/usr/local/include/eigen3/Eigen/LeastSquares;/usr/local/include/eigen3/Eigen/SVD;/usr/local/include/eigen3/Eigen/Eigen;/usr/local/include/eigen3/Eigen/IterativeLinearSolvers;/usr/local/include/eigen3/Eigen/Geometry;/usr/local/include/eigen3/Eigen/Householder;/usr/local/include/eigen3/Eigen/StdVector;/usr/local/include/eigen3/Eigen/LU;/usr/local/include/eigen3/Eigen/Dense;/usr/local/include/eigen3/Eigen/StdList")
FILE(INSTALL DESTINATION "/usr/local/include/eigen3/Eigen" TYPE FILE FILES
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/QtAlignedMalloc"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/PardisoSupport"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/UmfPackSupport"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/Eigen2Support"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/Array"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/OrderingMethods"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/Sparse"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/CholmodSupport"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/SparseCholesky"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/StdDeque"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/Jacobi"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/Core"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/PaStiXSupport"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/SparseCore"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/Cholesky"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/SuperLUSupport"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/QR"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/Eigenvalues"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/LeastSquares"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/SVD"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/Eigen"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/IterativeLinearSolvers"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/Geometry"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/Householder"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/StdVector"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/LU"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/Dense"
    "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/Eigen/StdList"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Devel")

IF(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  INCLUDE("/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/buildDir/Eigen/src/cmake_install.cmake")

ENDIF(NOT CMAKE_INSTALL_LOCAL_ONLY)

