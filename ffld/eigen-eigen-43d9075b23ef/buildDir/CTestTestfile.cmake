# CMake generated Testfile for 
# Source directory: /home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef
# Build directory: /home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/buildDir
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(Eigen)
SUBDIRS(doc)
SUBDIRS(test)
SUBDIRS(blas)
SUBDIRS(lapack)
SUBDIRS(unsupported)
SUBDIRS(demos)
SUBDIRS(scripts)
SUBDIRS(bench/spbench)
