# CMake generated Testfile for 
# Source directory: /home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/blas/testing
# Build directory: /home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/buildDir/blas/testing
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(sblat1 "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/blas/testing/runblastest.sh" "sblat1" "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/blas/testing/sblat1.dat")
ADD_TEST(sblat2 "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/blas/testing/runblastest.sh" "sblat2" "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/blas/testing/sblat2.dat")
ADD_TEST(sblat3 "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/blas/testing/runblastest.sh" "sblat3" "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/blas/testing/sblat3.dat")
ADD_TEST(dblat1 "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/blas/testing/runblastest.sh" "dblat1" "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/blas/testing/dblat1.dat")
ADD_TEST(dblat2 "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/blas/testing/runblastest.sh" "dblat2" "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/blas/testing/dblat2.dat")
ADD_TEST(dblat3 "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/blas/testing/runblastest.sh" "dblat3" "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/blas/testing/dblat3.dat")
ADD_TEST(cblat1 "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/blas/testing/runblastest.sh" "cblat1" "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/blas/testing/cblat1.dat")
ADD_TEST(cblat2 "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/blas/testing/runblastest.sh" "cblat2" "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/blas/testing/cblat2.dat")
ADD_TEST(cblat3 "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/blas/testing/runblastest.sh" "cblat3" "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/blas/testing/cblat3.dat")
ADD_TEST(zblat1 "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/blas/testing/runblastest.sh" "zblat1" "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/blas/testing/zblat1.dat")
ADD_TEST(zblat2 "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/blas/testing/runblastest.sh" "zblat2" "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/blas/testing/zblat2.dat")
ADD_TEST(zblat3 "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/blas/testing/runblastest.sh" "zblat3" "/home/kinauer/ros_workspace/src/ffld/eigen-eigen-43d9075b23ef/blas/testing/zblat3.dat")
