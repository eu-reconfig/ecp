cmake_minimum_required(VERSION 2.8.3)
project(ffld)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages

find_package(catkin REQUIRED COMPONENTS
  cv_bridge
  image_transport
  roscpp
  rospy
  sensor_msgs
  std_msgs
  reconfig_common_msgs
  ltl3_NTUA
)

## System dependencies are found with CMake's conventions
find_package(Boost REQUIRED COMPONENTS system)


OPTION(FFLD_HOGPYRAMID_FELZENSZWALB_FEATURES "Use Felzenszwalb's original features (slower and not as accurate, provided for compatibility only)." OFF)
OPTION(FFLD_HOGPYRAMID_DOUBLE "Use doubles instead of floats (slower, uses twice more memory, and the increase in precision is not necessarily useful)." ON)
SET(FFLD_HOGPYRAMID_DOUBLE ON)
MESSAGE("Double activated: ${FFLD_HOGPYRAMID_DOUBLE}")

# Select a default build configuration if none was chosen
IF(NOT CMAKE_BUILD_TYPE)
  SET(CMAKE_BUILD_TYPE "Release" CACHE STRING "Choose the type of build, options are: None (CMAKE_CXX_FLAGS or CMAKE_C_FLAGS used) Debug Release RelWithDebInfo MinSizeRel." FORCE)
ENDIF()


# Define the options
IF(FFLD_HOGPYRAMID_FELZENSZWALB_FEATURES)
  ADD_DEFINITIONS(-DFFLD_HOGPYRAMID_FELZENSZWALB_FEATURES)
ENDIF()

IF(FFLD_HOGPYRAMID_DOUBLE)
  ADD_DEFINITIONS(-DFFLD_HOGPYRAMID_DOUBLE)
ENDIF()

# There are no CMake Eigen package, so find it ourselves
FILE(GLOB EIGEN_ARCHIVE "eigen*")
message("archive entry: ${EIGEN_ARCHIVE}")
FIND_PATH(EIGEN_INCLUDE_DIR Eigen ${EIGEN_ARCHIVE} .)
IF(NOT EIGEN_INCLUDE_DIR)
  MESSAGE(FATAL_ERROR "Could not find Eigen.")
ENDIF()

FIND_PATH(FFTW_INCLUDE_DIR fftw3.h)
  FIND_PATH(FFTW_INCLUDE_DIR fftw3.h)
INCLUDE_DIRECTORIES(${FFTW_INCLUDE_DIR})
MESSAGE("The path to check: ${FFTW_INCLUDE_DIR}")

#FIND_PATH(FFTW_INCLUDE_DIR fftw3.h)
#FIND_PACKAGE(FFTW REQUIRED)
#IF(NOT FFTW_INCLUDES OR NOT FFTW_LIBRARIES)
#  MESSAGE(FATAL_ERROR "Could not find fftw3.")
#ENDIF()
#INCLUDE_DIRECTORIES(${FFTW_INCLUDES})
#MESSAGE("the path to check: ${FFTW_INCLUDES}")


IF(FFLD_HOGPYRAMID_DOUBLE)
  	FIND_LIBRARY(FFTW_LIBRARIES libfftw3.so.3)
ELSE()
  	FIND_LIBRARY(FFTW_LIBRARIES libfftw3f.so.3)
ENDIF()

#IF(NOT FFTW3_INCLUDE_DIR OR NOT FFTW3_LIBRARIES)
IF(NOT FFTW_INCLUDE_DIR OR NOT FFTW_LIBRARIES)
  MESSAGE(FATAL_ERROR "Could not find fftw3.")
ENDIF()

FIND_PACKAGE(JPEG REQUIRED)


FIND_PACKAGE(LibXml2 REQUIRED)
IF(LIBXML2_FOUND)
  ADD_DEFINITIONS(${LIBXML2_DEFINITIONS})
ENDIF()

# Not required, but stronlgy recommended on multi-core systems
FIND_PACKAGE(OpenMP)
IF(OPENMP_FOUND)
  SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
  SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
  SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_C_FLAGS}")
ENDIF()




SET(include_directory /ecp/ffld/include/)

# Also list the headers so that they are displayed along the .cpp files in the IDE
SET(HEADERS	${include_directory}Rectangle.h 
		${include_directory}JPEGImage.h 
		${include_directory}Object.h 
		${include_directory}Scene.h 
		${include_directory}HOGPyramid.h 
		${include_directory}Patchwork.h 
		${include_directory}Model.h 
		${include_directory}Mixture.h 
		${include_directory}Intersector.h 
		${include_directory}SimpleOpt.h
  	)

SET(SOURCES 	Rectangle.cpp 
	    	JPEGImage.cpp 
		Object.cpp 
		Scene.cpp 
		HOGPyramid.cpp 
		Patchwork.cpp 
		Model.cpp 
		Mixture.cpp
	)




## Uncomment this if the package has a setup.py. This macro ensures
## modules and global scripts declared therein get installed
## See http://ros.org/doc/api/catkin/html/user_guide/setup_dot_py.html
# catkin_python_setup()

################################################
## Declare ROS messages, services and actions ##
################################################

## To declare and build messages, services or actions from within this
## package, follow these steps:
## * Let MSG_DEP_SET be the set of packages whose message types you use in
##   your messages/services/actions (e.g. std_msgs, actionlib_msgs, ...).
## * In the file package.xml:
##   * add a build_depend and a run_depend tag for each package in MSG_DEP_SET
##   * If MSG_DEP_SET isn't empty the following dependencies might have been
##     pulled in transitively but can be declared for certainty nonetheless:
##     * add a build_depend tag for "message_generation"
##     * add a run_depend tag for "message_runtime"
## * In this file (CMakeLists.txt):
##   * add "message_generation" and every package in MSG_DEP_SET to
##     find_package(catkin REQUIRED COMPONENTS ...)
##   * add "message_runtime" and every package in MSG_DEP_SET to
##     catkin_package(CATKIN_DEPENDS ...)
##   * uncomment the add_*_files sections below as needed
##     and list every .msg/.srv/.action file to be processed
##   * uncomment the generate_messages entry below
##   * add every package in MSG_DEP_SET to generate_messages(DEPENDENCIES ...)

## Generate messages in the 'msg' folder
# add_message_files(
#   	FILES
#	rect.msg
#   	point.msg
#   	detect_res.msg
#   	det_list.msg
#	ObjClass.msg 
#	DetectResult2dPx.msg 
#	Point2d.msg 
#	Rectangle2d.msg
# )

## Generate services in the 'srv' folder
# add_service_files(
#   	FILES
#   	ObjectDetectPx.srv
# )

## Generate actions in the 'action' folder
# add_action_files(
#   FILES
#   Action1.action
#   Action2.action
# )

## Generate added messages and services with any dependencies listed here
# generate_messages(
#   	DEPENDENCIES
#	sensor_msgs   std_msgs
# )

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if you package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
#  INCLUDE_DIRS ${EIGEN_INCLUDE_DIR}
#  LIBRARIES ffld dpm_detector dpm_detector_srv
#  CATKIN_DEPENDS opencv roscpp rospy tf
#  DEPENDS system_lib
)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
# include_directories(include)
include_directories(
  ${catkin_INCLUDE_DIRS}
  ${PROJECT_SOURCE_DIR}/include
  ${EIGEN_INCLUDE_DIR}
  ${JPEG_INCLUDE_DIR} 
  ${LIBXML2_INCLUDE_DIR}
boost_1_56_0
)

## Declare a cpp library
# add_library(test_tf
#   src/${PROJECT_NAME}/test_tf.cpp
# )
IF(TRUE)
# define build targets after the catkin_package macro
#add_library(ffld ${SOURCES} ${HEADERS} ffld.cpp)
add_library(ffld ${SOURCES} ffld.cpp)
add_library( dpm_lib
#    src/dpm_detector.cpp
    src/dpm_detector_srv.cpp
)
target_link_libraries( dpm_lib ffld )
add_dependencies( dpm_lib ffld) 

add_executable( dpm_detector src/dpm_detector.cpp  )
target_link_libraries( dpm_detector dpm_lib ${catkin_LIBRARIES} ${Boost_LIBRARIES})
add_dependencies( dpm_detector dpm_lib ffld_lib_generate_messages_cpp )

add_executable( dpm_detector_srv src/dpm_detector_srv.cpp  )
target_link_libraries( dpm_detector_srv dpm_lib ${catkin_LIBRARIES} ${Boost_LIBRARIES} )
add_dependencies( dpm_detector_srv dpm_lib)

add_executable( service_stub src/service_stub.cpp )
target_link_libraries ( service_stub ${catkin_LIBRARIES} )

add_executable( async_detector src/async_detector.cpp )
target_link_libraries ( async_detector ${catkin_LIBRARIES} )

add_executable( async_knowledge src/async_knowledge.cpp )
target_link_libraries ( async_knowledge ${catkin_LIBRARIES} )


#INCLUDE_DIRECTORIES("/home/csl/catkin_ws/src/ecp/ffld/include")
ADD_EXECUTABLE(ffldexe ffld.cpp)
TARGET_LINK_LIBRARIES(ffldexe ffld)
ADD_DEPENDENCIES(ffldexe ffld)

TARGET_LINK_LIBRARIES(ffld ${FFTW_LIBRARIES} ${JPEG_LIBRARIES} ${LIBXML2_LIBRARIES} ${Boost_LIBRARIES})


ENDIF()

if(FALSE)
FILE(REMOVE vars.txt)
get_cmake_property(_variableNames VARIABLES)
foreach (_variableName ${_variableNames})
    FILE(APPEND vars.txt "${_variableName}=${${_variableName}}\n")
endforeach()
ENDIF()


#############
## Install ##
#############

# all install targets should use catkin DESTINATION variables
# See http://ros.org/doc/api/catkin/html/adv_user_guide/variables.html

## Mark executable scripts (Python etc.) for installation
## in contrast to setup.py, you can choose the destination
# install(PROGRAMS
#   scripts/my_python_script
#   DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

## Mark executables and/or libraries for installation
# install(TARGETS test_tf test_tf_node
#   ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

## Mark cpp header files for installation
# install(DIRECTORY include/${PROJECT_NAME}/
#   DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
#   FILES_MATCHING PATTERN "*.h"
#   PATTERN ".svn" EXCLUDE
# )

## Mark other files for installation (e.g. launch and bag files, etc.)
# install(FILES
#   # myfile1
#   # myfile2
#   DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
# )

#############
## Testing ##
#############

## Add gtest based cpp test target and link libraries
# catkin_add_gtest(${PROJECT_NAME}-test test/test_test_tf.cpp)
# if(TARGET ${PROJECT_NAME}-test)
#   target_link_libraries(${PROJECT_NAME}-test ${PROJECT_NAME})
# endif()

## Add folders to be run by python nosetests
# catkin_add_nosetests(test)
